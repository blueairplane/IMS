// Brian Lee (brianlee3742@gmail.com)
package ims.model;

public class Outsourced extends Part {
    private String companyName;

    /**
     * Constructor overloaded from Part class.
     *
     * @param id    the unique ID for this part
     * @param name  the name of this part
     * @param price the price of this part (in decimal form)
     * @param stock the inventory level of this part
     * @param min   the minimum amount of this part that must be stocked
     * @param max   the maximum amount of this part that can be stocked
     */
    public Outsourced(int id, String name, double price, int stock, int min, int max) {
        super(id, name, price, stock, min, max);
    }

    /**
     * Constructor that sets the name of the company that makes this part.
     *
     * @param id          the unique ID for this part
     * @param name        the name of this part
     * @param price       the price of this part (in decimal form)
     * @param stock       the inventory level of this part
     * @param min         the minimum amount of this part that must be stocked
     * @param max         the maximum amount of this part that can be stocked
     * @param companyName the name of the company that makes this part
     */
    public Outsourced(int id, String name, double price, int stock, int min, int max, String companyName) {
        this(id, name, price, stock, min, max);
        this.companyName = companyName;
    }

    // setters and getters

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
