// Brian Lee (brianlee3742@gmail.com)
package ims.model;

/**
 * Thrown when a Product does not have any associated Parts. Used internally by Inventory.
 */
public class NoAssociatedPartsException extends Exception {
    public NoAssociatedPartsException() {
        super();
    }

    public NoAssociatedPartsException(String message) {
        super(message);
    }

    public NoAssociatedPartsException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoAssociatedPartsException(Throwable cause) {
        super(cause);
    }

    protected NoAssociatedPartsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
