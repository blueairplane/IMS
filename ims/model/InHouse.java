// Brian Lee (brianlee3742@gmail.com)
package ims.model;

public class InHouse extends Part {
    private int machineId;

    /**
     * Constructor overloaded from Part class.
     *
     * @param id    the unique ID for this part
     * @param name  the name of this part
     * @param price the price of this part (in decimal form)
     * @param stock the inventory level of this part
     * @param min   the minimum amount of this part that must be stocked
     * @param max   the maximum amount of this part that can be stocked
     */
    public InHouse(int id, String name, double price, int stock, int min, int max) {
        super(id, name, price, stock, min, max);
    }

    /**
     * Constructor that sets the ID of the machine that makes this part.
     *
     * @param id        the unique ID for this part
     * @param name      the name of this part
     * @param price     the price of this part (in decimal form)
     * @param stock     the inventory level of this part
     * @param min       the minimum amount of this part that must be stocked
     * @param max       the maximum amount of this part that can be stocked
     * @param machineId the ID number for the machine that made this part
     */
    public InHouse(int id, String name, double price, int stock, int min, int max, int machineId) {
        this(id, name, price, stock, min, max);
        this.machineId = machineId;
    }

    // setters & getters

    public int getMachineId() {
        return machineId;
    }

    public void setMachineId(int machineId) {
        this.machineId = machineId;
    }
}
