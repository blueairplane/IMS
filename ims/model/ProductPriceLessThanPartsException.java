// Brian Lee (brianlee3742@gmail.com)
package ims.model;

/**
 * Thrown when a Product's price is less than the sum of the prices of its associated Parts. Used internally by Inventory.
 */
public class ProductPriceLessThanPartsException extends Exception {
    public ProductPriceLessThanPartsException() {
        super();
    }

    public ProductPriceLessThanPartsException(String message) {
        super(message);
    }

    public ProductPriceLessThanPartsException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProductPriceLessThanPartsException(Throwable cause) {
        super(cause);
    }

    protected ProductPriceLessThanPartsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
