// Brian Lee (brianlee3742@gmail.com)
package ims.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Inventory {
    /**
     * List of all parts in the inventory.
     */
    private static ObservableList<Part> allParts = FXCollections.observableArrayList();
    /**
     * List of all products in the inventory.
     */
    private static ObservableList<Product> allProducts = FXCollections.observableArrayList();

    /**
     * Adds a Part to this Inventory's parts list
     *
     * @param newPart the Part to be added
     */
    public static void addPart(Part newPart) {
        allParts.add(newPart);
    }

    /**
     * Adds a Product to this Inventory's products list
     *
     * @param newProduct the Product to be added
     * @throws NoAssociatedPartsException         if {@code newProduct} has no associated parts
     * @throws ProductPriceLessThanPartsException if sum of prices of associated parts of {@code newProduct} is greater
     *                                            than price of {@code newProduct}
     */
    public static void addProduct(Product newProduct) throws NoAssociatedPartsException, ProductPriceLessThanPartsException {
        if (newProduct.getAllAssociatedParts().size() < 1) {
            throw new NoAssociatedPartsException("A product must have at least one associated part.");
        }
        double sum = 0;
        for (Part part : newProduct.getAllAssociatedParts()) {
            sum += part.getPrice();
        }
        if (sum - newProduct.getPrice() > 0.0001) {
            throw new ProductPriceLessThanPartsException("The sum of the part prices cannot be greater than the product price.");
        }
        allProducts.add(newProduct);
    }

    /**
     * Finds a part by its unique ID.
     *
     * @param partId the unique ID for the Part
     * @return the Part whose ID is {@code partId}
     * @throws NoSuchElementException if there is no part with ID {@code partId}
     */
    public static Part lookupPart(int partId) throws NoSuchElementException {
        for (Part part : allParts) {
            if (part.getId() == partId) {
                return part;
            }
        }
        throw new NoSuchElementException("No Part found with ID " + partId);
    }

    /**
     * Finds a product by its unique ID.
     *
     * @param productId the unique ID for the Product
     * @return the Product whose ID is {@code productId}
     * @throws NoSuchElementException if there is no Product with ID {@code productID}
     */
    public static Product lookupProduct(int productId) throws NoSuchElementException {
        for (Product product : allProducts) {
            if (product.getId() == productId) {
                return product;
            }
        }
        throw new NoSuchElementException("No Product found with ID " + productId);
    }

    /**
     * Finds all parts whose name is a match or partial match of {@code partName}. If no matching parts are found,
     * returns an empty list of parts.
     *
     * @param partName the name (or portion of a name) of the part(s)
     * @return a list of the matching parts (may be empty if no parts match)
     */
    public static ObservableList<Part> lookupPart(String partName) {
        ObservableList<Part> foundParts = FXCollections.observableArrayList();
        for (Part part : allParts) {
            if (part.getName().toLowerCase().contains(partName.toLowerCase())) {
                foundParts.add(part);
            }
        }
        return foundParts;
    }

    /**
     * Finds all products whose name is a match or partial match of {@code productName}. If no matching products
     * are found, returns an empty list of products.
     *
     * @param productName the name (or portion of a name) of the product(s)
     * @return a list of the matching products (may be empty if no products match)
     */
    public static ObservableList<Product> lookupProduct(String productName) {
        ObservableList<Product> foundProducts = FXCollections.observableArrayList();
        for (Product product : allProducts) {
            if (product.getName().toLowerCase().contains(productName.toLowerCase())) {
                foundProducts.add(product);
            }
        }
        return foundProducts;
    }

    /**
     * Updates the part at the specified location in the parts list.
     *
     * @param index        the index of the Part to be changed
     * @param selectedPart the updated Part
     */
    public static void updatePart(int index, Part selectedPart) {
        allParts.set(index, selectedPart);
    }

    /**
     * Updates the product at the specified location in the products list
     *
     * @param index      the index of the Product to be changed
     * @param newProduct the updated Product
     */
    public static void updateProduct(int index, Product newProduct) throws NoAssociatedPartsException, ProductPriceLessThanPartsException {
        if (newProduct.getAllAssociatedParts().size() < 1) {
            throw new NoAssociatedPartsException("A product must have at least one associated part.");
        }
        double sum = 0;
        for (Part part : newProduct.getAllAssociatedParts()) {
            sum += part.getPrice();
        }
        if (sum - newProduct.getPrice() > 0.0001) {
            throw new ProductPriceLessThanPartsException("The sum of the part prices cannot be greater than the product price.");
        }
        allProducts.set(index, newProduct);
    }

    /**
     * Removes the indicated part from the parts list.
     *
     * @param selectedPart the Part to be removed
     * @return true if the Part could be removed, false otherwise
     */
    public static boolean deletePart(Part selectedPart) {
        return allParts.remove(selectedPart);
    }

    /**
     * Removes the indicated product from the products list.
     *
     * @param selectedProduct the Product to be removed
     * @return true if the Product could be removed, false otherwise
     */
    public static boolean deleteProduct(Product selectedProduct) {
        return allProducts.remove(selectedProduct);
    }

    /**
     * Gets the list of all parts in this inventory.
     *
     * @return the parts list
     */
    public static ObservableList<Part> getAllParts() {
        return allParts;
    }

    /**
     * Gets the list of all products in this inventory.
     *
     * @return the products list
     */
    public static ObservableList<Product> getAllProducts() {
        return allProducts;
    }

    public static int generatePartId() {
        List<Integer> usedIds = new ArrayList<>();
        for (Part part : getAllParts()) {
            usedIds.add(part.getId());
        }
        int newId = 1;
        while (usedIds.contains(newId)) {
            newId++;
        }
        return newId;
    }

    public static int generateProductId() {
        List<Integer> usedIds = new ArrayList<>();
        for (Product product : getAllProducts()) {
            usedIds.add(product.getId());
        }
        int newId = 1;
        while (usedIds.contains(newId)) {
            newId++;
        }
        return newId;
    }
}
