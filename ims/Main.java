// Brian Lee (brianlee3742@gmail.com)
package ims;

import ims.view_controller.*;
import javafx.application.Application;
import javafx.stage.Stage;

import java.util.HashMap;

public class Main extends Application {
    /**
     * HashMap of the different controllers for this application. An individual controller can be accessed by
     * the name of its FXML file (without the .fxml extension). Should be populated in this class so the FXML
     * is injected for the different controllers.
     */
    private HashMap<String, Controller> controllers = new HashMap<>();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        String windowTitle = "Inventory Management System";
        MainScreenController mainScreenController = new MainScreenController(controllers, "MainScreen", windowTitle);
        controllers.put("MainScreen", mainScreenController);
        AddPartController addPartController = new AddPartController(controllers, "AddPart", windowTitle);
        controllers.put("AddPart", addPartController);
        ModifyPartController modifyPartController = new ModifyPartController(controllers, "ModifyPart", windowTitle);
        controllers.put("ModifyPart", modifyPartController);
        AddProductController addProductController = new AddProductController(controllers, "AddProduct", windowTitle);
        controllers.put("AddProduct", addProductController);
        ModifyProductController modifyProductController = new ModifyProductController(controllers, "ModifyProduct", windowTitle);
        controllers.put("ModifyProduct", modifyProductController);

        mainScreenController.showStage();
    }
}
