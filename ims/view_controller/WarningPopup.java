// Brian Lee (brianlee3742@gmail.com)
package ims.view_controller;

import javafx.scene.control.Alert;

public final class WarningPopup {
    private WarningPopup() {
    }

    /**
     * Shows an alert window with the given warning message.
     *
     * @param text the warning message
     */
    public static void showMessage(String text) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(null);
        alert.setHeaderText(null);
        alert.setContentText(text);
        alert.showAndWait();
    }
}
