// Brian Lee (brianlee3742@gmail.com)
package ims.view_controller;

import ims.model.Inventory;
import ims.model.Part;
import ims.model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.HashMap;

public class ModifyProductController extends Controller {
    @FXML
    private TextField partSearchField;
    @FXML
    private TableView<Part> availablePartsTable;
    @FXML
    private TableColumn<Part, Integer> availablePartIdCol;
    @FXML
    private TableColumn<Part, String> availablePartNameCol;
    @FXML
    private TableColumn<Part, Integer> availablePartStockCol;
    @FXML
    private TableColumn<Part, Double> availablePartPriceCol;
    @FXML
    private TableView<Part> includedPartsTable;
    @FXML
    private TableColumn<Part, Integer> includedPartIdCol;
    @FXML
    private TableColumn<Part, String> includedPartNameCol;
    @FXML
    private TableColumn<Part, Integer> includedPartStockCol;
    @FXML
    private TableColumn<Part, Double> includedPartPriceCol;
    @FXML
    private TextField id;
    @FXML
    private TextField name;
    @FXML
    private TextField stock;
    @FXML
    private TextField price;
    @FXML
    private TextField min;
    @FXML
    private TextField max;

    private ObservableList<Part> availableParts = FXCollections.observableArrayList();
    private ObservableList<Part> includedParts = FXCollections.observableArrayList();
    private int selectedIndex;

    /**
     * Sets up the tables in this window.
     *
     * @see ims.view_controller.Controller#Controller(HashMap, String, String)
     */
    public ModifyProductController(HashMap<String, Controller> controllers, String name, String windowTitle) throws IOException {
        super(controllers, name, windowTitle);

        // setup available parts table:
        availablePartIdCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        availablePartNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        availablePartStockCol.setCellValueFactory(new PropertyValueFactory<>("stock"));
        availablePartPriceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        availablePartPriceCol.setCellFactory(tc -> new TableCell<Part, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(NumberFormat.getCurrencyInstance().format(item));
                }
            }
        });
        availablePartsTable.setItems(availableParts);

        // setup included parts table:
        includedPartIdCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        includedPartNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        includedPartStockCol.setCellValueFactory(new PropertyValueFactory<>("stock"));
        includedPartPriceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        includedPartPriceCol.setCellFactory(tc -> new TableCell<Part, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(NumberFormat.getCurrencyInstance().format(item));
                }
            }
        });
        includedPartsTable.setItems(includedParts);
    }

    /**
     * Resets the window for the next use. Clears all editable fields and the included parts list.
     */
    private void clearFields() {
        name.clear();
        price.clear();
        stock.clear();
        min.clear();
        max.clear();
        includedParts.clear();
    }

    /**
     * Sets up the window with the selected product's information. Fills all relevant fields, populates the
     * available and included parts tables, and stores the product's location in the inventory's products list.
     *
     * @param selectedIndex   the index of the selected product in the inventory's products list
     * @param selectedProduct the product that will be modified
     */
    public void setSelectedProductInfo(int selectedIndex, Product selectedProduct) {
        this.selectedIndex = selectedIndex;

        id.setText(String.valueOf(selectedProduct.getId()));
        name.setText(selectedProduct.getName());
        stock.setText(String.valueOf(selectedProduct.getStock()));
        price.setText(String.valueOf(selectedProduct.getPrice()));
        min.setText(String.valueOf(selectedProduct.getMin()));
        max.setText(String.valueOf(selectedProduct.getMax()));

        availableParts.setAll(Inventory.getAllParts());
        includedParts.addAll(selectedProduct.getAllAssociatedParts());
    }

    @Override
    public void showStage() {
        stage.setResizable(false);
        stage.showAndWait();
    }

    /**
     * Filters the available parts table to show only parts with a name that is a match or partial match of
     * the text entered in the search field.
     *
     * @param actionEvent the event that triggered this call. Unused.
     */
    @FXML
    public void searchPartsClicked(ActionEvent actionEvent) {
        ObservableList<Part> foundParts = FXCollections.observableArrayList();
        for (Part part : availableParts) {
            if (part.getName().contains(partSearchField.getText())) {
                foundParts.add(part);
            }
        }
        availablePartsTable.setItems(foundParts);
    }

    /**
     * Adds the selected part from the available parts table to this product's included parts.
     *
     * @param actionEvent the event that triggered this call. Unused.
     */
    @FXML
    public void addPartClicked(ActionEvent actionEvent) {
        Part selectedPart = availablePartsTable.getSelectionModel().getSelectedItem();
        if (selectedPart != null) {
            includedParts.add(selectedPart);
        }
    }

    /**
     * Removes the selected part from this part's included parts list.
     *
     * @param actionEvent the event that triggered this call. Unused.
     */
    @FXML
    public void deletePartClicked(ActionEvent actionEvent) {
        Part selectedPart = includedPartsTable.getSelectionModel().getSelectedItem();
        includedParts.remove(selectedPart);
    }

    /**
     * Saves the entered product to the inventory. Validates that all fields are filled and of the proper formats before
     * saving. If any are missing or in the wrong format, displays a warning message and makes no changes. If all
     * fields are filled and correctly formatted, adds the new product to the inventory and closes the window.
     *
     * @param actionEvent the event that triggered this call. Unused.
     */
    @FXML
    public void save(ActionEvent actionEvent) {
        if (name.getLength() == 0 || stock.getLength() == 0 || price.getLength() == 0 ||
                min.getLength() == 0 || max.getLength() == 0) {
            WarningPopup.showMessage("All fields must be filled to save the product.");
        } else {
            int newId = Integer.parseInt(id.getText());
            double newPrice;
            try {
                newPrice = Double.parseDouble(price.getText());
            } catch (NumberFormatException e) {
                WarningPopup.showMessage("Price must be in decimal format.");
                return;
            }
            int newStock;
            try {
                newStock = Integer.parseInt(stock.getText());
            } catch (NumberFormatException e) {
                WarningPopup.showMessage("Inv must be a whole number.");
                return;
            }
            int newMin;
            try {
                newMin = Integer.parseInt(min.getText());
            } catch (NumberFormatException e) {
                WarningPopup.showMessage("Min must be a whole number.");
                return;
            }
            int newMax;
            try {
                newMax = Integer.parseInt(max.getText());
            } catch (NumberFormatException e) {
                WarningPopup.showMessage("Max must be a whole number.");
                return;
            }

            Product newProduct = new Product(newId, name.getText(), newPrice, newStock, newMin, newMax);
            for (Part part : includedParts) {
                newProduct.addAssociatedPart(part);
            }
            try {
                Inventory.updateProduct(selectedIndex, newProduct);
                clearFields();
                stage.close();
            } catch (Exception e) {
                WarningPopup.showMessage(e.getMessage());
            }
        }
    }

    /**
     * Resets and closes the window.
     *
     * @param actionEvent the event that triggered this call. Unused.
     */
    @FXML
    public void cancel(ActionEvent actionEvent) {
        clearFields();
        stage.close();
    }
}
