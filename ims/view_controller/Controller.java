// Brian Lee (brianlee3742@gmail.com)
package ims.view_controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.HashMap;

/**
 * Base class for controllers that can pass information to each other and switch windows.
 */
public class Controller {
    /**
     * Stage for this window.
     */
    protected final Stage stage;
    /**
     * HashMap of the different controllers for this application. An individual controller can be accessed by
     * the name of its FXML file (without the .fxml extension).
     */
    protected final HashMap<String, Controller> controllers;

    /**
     * Constructor with default (empty string) windowTitle
     *
     * @param controllers the shared HashMap of controllers so this controller can pass information to another
     *                    controller in this application
     * @param name        the name (without the .fxml extension) of this FXML file controlled by this controller. Also used
     *                    as the lookup String for {@code controllers}
     * @throws IOException possibly thrown by FXMLLoader
     */
    public Controller(HashMap<String, Controller> controllers, String name) throws IOException {
        this(controllers, name, "");
    }

    /**
     * Constructor that explicitly sets the window title
     *
     * @param controllers the shared HashMap of controllers so this controller can pass information to another
     *                    controller in this application
     * @param name        the name (without the .fxml extension) of this FXML file controlled by this controller. Also used
     *                    as the lookup String for {@code controllers}
     * @param windowTitle text that will appear in this window's title bar
     * @throws IOException possibly thrown by FXMLLoader
     */
    public Controller(HashMap<String, Controller> controllers, String name, String windowTitle) throws IOException {
        this.controllers = controllers;

        stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource(name + ".fxml"));
        loader.setController(this);
        stage.setScene(new Scene(loader.load()));
        stage.setTitle(windowTitle);
    }

    /**
     * Makes this window visible.
     */
    public void showStage() {
        stage.show();
    }

    /**
     * Receives data from another controller for use in this controller. The data is received as an Object so the
     * user of this class is not restricted to a specific type of data. It will most likely have to be cast to be used.
     *
     * @param data the data passed to this controller
     */
    public void update(Object data) {
        return;
    }
}
