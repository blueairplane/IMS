// Brian Lee (brianlee3742@gmail.com)
package ims.view_controller;

import ims.model.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.HashMap;

public class MainScreenController extends Controller {
    @FXML
    private TextField partSearchField;
    @FXML
    private TableView<Part> partTable;
    @FXML
    private TableColumn<Part, Integer> partIdCol;
    @FXML
    private TableColumn<Part, String> partNameCol;
    @FXML
    private TableColumn<Part, Integer> partInvCol;
    @FXML
    private TableColumn<Part, Double> partPriceCol;

    @FXML
    private TextField productSearchField;
    @FXML
    private TableView<Product> productTable;
    @FXML
    private TableColumn<Product, Integer> productIdCol;
    @FXML
    private TableColumn<Product, String> productNameCol;
    @FXML
    private TableColumn<Product, Integer> productInvCol;
    @FXML
    private TableColumn<Product, Double> productPriceCol;

    /**
     * Sets up the tables in this window.
     *
     * @see ims.view_controller.Controller#Controller(HashMap, String, String)
     */
    public MainScreenController(HashMap<String, Controller> controllers, String name, String windowTitle) throws IOException {
        super(controllers, name, windowTitle);

        partIdCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        partNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        partInvCol.setCellValueFactory(new PropertyValueFactory<>("stock"));
        partPriceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        partPriceCol.setCellFactory(tc -> new TableCell<Part, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(NumberFormat.getCurrencyInstance().format(item));
                }
            }
        });
        partTable.setItems(Inventory.getAllParts());

        productIdCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        productNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        productInvCol.setCellValueFactory(new PropertyValueFactory<>("stock"));
        productPriceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        productPriceCol.setCellFactory(tc -> new TableCell<Product, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(NumberFormat.getCurrencyInstance().format(item));
                }
            }
        });
        productTable.setItems(Inventory.getAllProducts());

        /* sample parts and products for testing */
        Inventory.addPart(new InHouse(1, "foo", 42.37, 42, 12, 121, 876));
        Inventory.addPart(new InHouse(2, "foobar", 2.37, 37, 12, 99, 7659));
        Inventory.addPart(new Outsourced(3, "towel", 4.37, 1, 1, 7, "HHGTTG"));
        Inventory.addPart(new InHouse(4, "trowel", 4.00, 3, 1, 12, 8383));

        try {
            Product product1 = new Product(1, "Fizzbuzz", 54.45, 2, 1, 5);
            product1.addAssociatedPart(Inventory.lookupPart(1));
            product1.addAssociatedPart(Inventory.lookupPart(3));
            Inventory.addProduct(product1);
            Product product2 = new Product(2, "Razzmatazz", 12.72, 11, 1, 25);
            product2.addAssociatedPart(Inventory.lookupPart(4));
            product2.addAssociatedPart(Inventory.lookupPart(3));
            Inventory.addProduct(product2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        /* end samples */
    }

    @Override
    public void showStage() {
        stage.setResizable(false);
        super.showStage();
    }

    /**
     * Forces the tables to refresh their contents. There seems to be a bug in JavaFX that causes a TableView to not
     * always update on certain changes to the underlying data. The search methods happen to change the data in a way
     * that causes the TableView to update its contents, so this is the simplest workaround. It also preserves the
     * current filtered view of the tables as long as nothing has changed the text in the search fields.
     */
    private void refreshTables() {
        searchParts(null);
        searchProducts(null);
    }

    /**
     * Opens the Add Part window.
     *
     * @param actionEvent the event that triggered this call. Unused.
     */
    @FXML
    public void addPartClicked(ActionEvent actionEvent) {
        AddPartController addPartController = (AddPartController) controllers.get("AddPart");
        stage.hide();
        addPartController.showStage();
        stage.show();
        refreshTables();
    }

    /**
     * Opens the Add Product window.
     *
     * @param actionEvent the event that triggered this call. Unused.
     */
    @FXML
    public void addProductClicked(ActionEvent actionEvent) {
        AddProductController addProductController = (AddProductController) controllers.get("AddProduct");
        stage.hide();
        addProductController.showStage();
        stage.show();
        refreshTables();
    }

    /**
     * Opens the Modify Part window and sends it the index of and {@code Part} object of the selected part.
     *
     * @param actionEvent the event that triggered this call. Unused.
     */
    @FXML
    public void modifyPartClicked(ActionEvent actionEvent) {
        ModifyPartController modifyPartController = (ModifyPartController) controllers.get("ModifyPart");
        int selectedIndex = partTable.getSelectionModel().getSelectedIndex();
        Part selectedPart = partTable.getSelectionModel().getSelectedItem();
        if (selectedIndex != -1 && selectedPart != null) {
            modifyPartController.setSelectedPartInfo(selectedIndex, selectedPart);
            stage.hide();
            modifyPartController.showStage();
            stage.show();
            refreshTables();
        }
    }

    /**
     * Opens the Modify Product window and sends it the index of and {@code Product} object of the selected product.
     *
     * @param actionEvent the event that triggered this call. Unused.
     */
    @FXML
    public void modifyProductClicked(ActionEvent actionEvent) {
        ModifyProductController modifyProductController = (ModifyProductController) controllers.get("ModifyProduct");
        int selectedIndex = productTable.getSelectionModel().getSelectedIndex();
        Product selectedProduct = productTable.getSelectionModel().getSelectedItem();
        if (selectedIndex != -1 && selectedProduct != null) {
            modifyProductController.setSelectedProductInfo(selectedIndex, selectedProduct);
            stage.hide();
            modifyProductController.showStage();
            stage.show();
            refreshTables();
        }
    }

    /**
     * Removes the selected part from the inventory.
     *
     * @param actionEvent the event that triggered this call. Unused.
     */
    @FXML
    public void deletePartClicked(ActionEvent actionEvent) {
        Part selectedPart = partTable.getSelectionModel().getSelectedItem();
        Inventory.deletePart(selectedPart);
        refreshTables();
    }

    /**
     * Removes the selected product from the inventory.
     *
     * @param actionEvent the event that triggered this call. Unused.
     */
    @FXML
    public void deleteProductClicked(ActionEvent actionEvent) {
        Product selectedProduct = productTable.getSelectionModel().getSelectedItem();
        Inventory.deleteProduct(selectedProduct);
        refreshTables();
    }

    /**
     * Filters the parts list to show only matches or partial matches of the text in the search field.
     *
     * @param actionEvent the event that triggered this call. Unused.
     */
    @FXML
    public void searchParts(ActionEvent actionEvent) {
        partTable.setItems(Inventory.lookupPart(partSearchField.getText()));
    }

    /**
     * Filters the products list to show only matches or partial matches of the text in the search field.
     *
     * @param actionEvent the event that triggered this call. Unused.
     */
    @FXML
    public void searchProducts(ActionEvent actionEvent) {
        productTable.setItems(Inventory.lookupProduct(productSearchField.getText()));
    }

    @FXML
    public void exit(ActionEvent actionEvent) {
        javafx.application.Platform.exit();
    }
}
