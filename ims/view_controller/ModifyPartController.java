// Brian Lee (brianlee3742@gmail.com)
package ims.view_controller;

import ims.model.InHouse;
import ims.model.Inventory;
import ims.model.Outsourced;
import ims.model.Part;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.util.HashMap;

public class ModifyPartController extends Controller {
    @FXML
    private RadioButton inHouseButton;
    @FXML
    private RadioButton outsourcedButton;
    @FXML
    private TextField id;
    @FXML
    private TextField name;
    @FXML
    private TextField stock;
    @FXML
    private TextField price;
    @FXML
    private TextField min;
    @FXML
    private TextField max;
    @FXML
    private Label companyNameLabel;
    @FXML
    private TextField companyName;
    @FXML
    private Label machineIdLabel;
    @FXML
    private TextField machineId;

    private int selectedIndex;

    /**
     * @see ims.view_controller.Controller#Controller(HashMap, String, String)
     */
    public ModifyPartController(HashMap<String, Controller> controllers, String name, String windowTitle) throws IOException {
        super(controllers, name, windowTitle);
    }

    /**
     * Resets the window for the next use by clearing all editable fields.
     */
    private void clearFields() {
        name.clear();
        price.clear();
        stock.clear();
        companyName.clear();
        machineId.clear();
        min.clear();
        max.clear();
    }

    /**
     * Sets up the window with the selected part's information. Fills all relevant fields and stores the part's
     * location in the inventory's parts list.
     *
     * @param selectedIndex the index of the selected part in the inventory's parts list
     * @param selectedPart  the part that will be modified
     */
    public void setSelectedPartInfo(int selectedIndex, Part selectedPart) {
        this.selectedIndex = selectedIndex;
        id.setText(String.valueOf(selectedPart.getId()));
        name.setText(selectedPart.getName());
        stock.setText(String.valueOf(selectedPart.getStock()));
        price.setText(String.valueOf(selectedPart.getPrice()));
        min.setText(String.valueOf(selectedPart.getMin()));
        max.setText(String.valueOf(selectedPart.getMax()));
        if (selectedPart instanceof InHouse) {
            inHouseButton.setSelected(true);
            selectInHouse(null);
            machineId.setText(String.valueOf(((InHouse) selectedPart).getMachineId()));
        } else {
            outsourcedButton.setSelected(true);
            selectOutsourced(null);
            companyName.setText(((Outsourced) selectedPart).getCompanyName());
        }
    }

    @Override
    public void showStage() {
        stage.setResizable(false);
        stage.showAndWait();
    }

    /**
     * Switches to an in-house part view.
     *
     * @param actionEvent the event that triggered this call. Unused.
     */
    @FXML
    public void selectInHouse(ActionEvent actionEvent) {
        companyNameLabel.setVisible(false);
        companyName.setVisible(false);
        machineIdLabel.setVisible(true);
        machineId.setVisible(true);
    }

    /**
     * Switches to an outsourced part view.
     *
     * @param actionEvent the event that triggered this call. Unused.
     */
    @FXML
    public void selectOutsourced(ActionEvent actionEvent) {
        companyNameLabel.setVisible(true);
        companyName.setVisible(true);
        machineIdLabel.setVisible(false);
        machineId.setVisible(false);
    }

    /**
     * Saves the entered part to the inventory. Validates that all fields are filled and of the proper formats before
     * saving. If any are missing or in the wrong format, displays a warning message and makes no changes. If all
     * fields are filled and correctly formatted, adds the new part to the inventory and closes the window.
     *
     * @param actionEvent the event that triggered this call. Unused.
     */
    @FXML
    public void save(ActionEvent actionEvent) {
        if (name.getLength() == 0 || stock.getLength() == 0 || price.getLength() == 0 ||
                min.getLength() == 0 || max.getLength() == 0 ||
                (companyName.isVisible() && companyName.getLength() == 0) ||
                (machineId.isVisible() && machineId.getLength() == 0)) {
            WarningPopup.showMessage("All fields must be filled to save the part.");
            return;
        }

        Part newPart;
        int newId = Integer.parseInt(id.getText());
        double newPrice;
        try {
            newPrice = Double.parseDouble(price.getText());
        } catch (NumberFormatException e) {
            WarningPopup.showMessage("Price must be in decimal format.");
            return;
        }
        int newStock;
        try {
            newStock = Integer.parseInt(stock.getText());
        } catch (NumberFormatException e) {
            WarningPopup.showMessage("Inv must be a whole number.");
            return;
        }
        int newMin;
        try {
            newMin = Integer.parseInt(min.getText());
        } catch (NumberFormatException e) {
            WarningPopup.showMessage("Min must be a whole number.");
            return;
        }
        int newMax;
        try {
            newMax = Integer.parseInt(max.getText());
        } catch (NumberFormatException e) {
            WarningPopup.showMessage("Max must be a whole number.");
            return;
        }

        if (inHouseButton.isSelected()) {
            int newMachineId;
            try {
                newMachineId = Integer.parseInt(machineId.getText());
            } catch (NumberFormatException e) {
                WarningPopup.showMessage("Machine ID must be a whole number.");
                return;
            }
            newPart = new InHouse(newId, name.getText(), newPrice, newStock, newMin, newMax, newMachineId);
        } else {
            newPart = new Outsourced(newId, name.getText(), newPrice, newStock, newMin, newMax, companyName.getText());
        }

        Inventory.updatePart(selectedIndex, newPart);

        clearFields();
        stage.close();
    }

    /**
     * Resets and closes the window.
     *
     * @param actionEvent the event that triggered this call. Unused.
     */
    @FXML
    public void cancel(ActionEvent actionEvent) {
        clearFields();
        stage.close();
    }
}
